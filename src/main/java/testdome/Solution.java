package testdome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    public int solution1(int N) {
        final int BASE = 10;

        List<Integer> digits = new ArrayList<>();
        while (0 < N) {
            digits.add(N % BASE);
            N /= BASE;
        }

        for (int i = 0; i < digits.size(); ++i) {
            int digit = digits.get(i);
            if (0 < digit) {
                int pos = i;
                for (int j = i + 1; j < digits.size(); ++j) {
                    if ((digits.get(j) + 1) < BASE) {
                        pos = j;
                        break;
                    }
                }
                digits.set(i, digit - 1);
                if (i == pos) {
                    digits.add(1);
                } else {
                    digits.set(pos,  digits.get(pos) + 1);
                }
                int sum = 0;
                for (int j = 0; j < pos; ++j) {
                    sum += digits.get(j);
                    digits.set(j, 0);
                }
                pos = 0;
                while (0 < sum) {
                    int digit2 = Math.min(BASE - 1, sum);
                    digits.set(pos, digit2);
                    ++pos;
                    sum -= digit2;
                }
                break;
            }
        }

        int result = 0;
        for (int i = digits.size() - 1; 0 <= i; --i) {
            result *= BASE;
            result += digits.get(i);
        }

        return result;
    }

    public int solution2(int[] A) {
        boolean[] flags = new boolean[A.length];
        int maxLength = 0;
        for (int i = 0; i < A.length; ++i) {
            if (flags[i]) {
                continue;
            }
            int pos = i;
            int length = 0;
            while (!flags[pos]) {
                flags[i] = true;
                ++length;
                pos = A[pos];
            }
            maxLength = Math.max(maxLength, length);
        }
        return maxLength;
    }

    public int solution3(int[] A) {
        final int BITS = 32;

        int[] counts = new int[BITS];

        for (int i = 0; i < BITS; ++i) {
            int mask = 1 << i;
            for (int j = 0; j < A.length; ++j) {
                if (0 != (mask & A[j])) {
                    ++counts[i];
                }
            }
        }

        return Arrays.stream(counts).max().getAsInt();
    }
}
