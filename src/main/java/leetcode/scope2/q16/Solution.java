package leetcode.scope2.q16;

import java.util.Arrays;

public class Solution {
    public int threeSumClosest(int[] nums, int target) {
    	Arrays.sort(nums);
    	int closest = nums[0] + nums[1] + nums[2];
		for (int i = 0; i < (nums.length - 2); ++i) {
			int value = nums[i];
			int diff = target - value;
			
			int left = i + 1;
			int right = nums.length - 1;
			while (left < right) {
				int sum = nums[left] + nums[right];
				if (sum < diff) {
					++left;
				}
				else if (diff < sum) {
					--right;
				}
				else {
					return target;
				}
				if (Math.abs(sum - diff) < Math.abs(closest - target)) {
					closest = value + sum;
				}
			}
			while ((value == nums[i + 1]) && ((i + 1) < (nums.length - 2))) {
				++i;
			}
		}
        return closest;
    }
}
