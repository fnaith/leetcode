package leetcode.scope2.q18;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
    	Arrays.sort(nums);
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		for (int i = 0; i < (nums.length - 3); ++i) {
			int value1 = nums[i];
			for (int j = i + 1; j < (nums.length - 2); ++j) {
				int value2 = nums[j];
				int diff = target - value1 - value2;
				
				int left = j + 1;
				int right = nums.length - 1;
				while (left < right) {
					int sum = nums[left] + nums[right];
					if (sum < diff) {
						++left;
					}
					else if (diff < sum) {
						--right;
					}
					else {
						results.add(Arrays.asList(value1, value2, nums[left], nums[right]));
						while ((left < right) && (nums[left] == nums[left + 1])) {
							++left;
						}
						++left;
						while ((left < right) && (nums[right] == nums[right - 1])) {
							--right;
						}
						--right;
					}
				}
				while ((value2 == nums[j + 1]) && ((j + 1) < (nums.length - 2))) {
					++j;
				}
			}
			while ((value1 == nums[i + 1]) && ((i + 1) < (nums.length - 3))) {
				++i;
			}
		}
        return results;
    }
}
