package leetcode.scope2.q15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
    	Arrays.sort(nums);
    	int target = 0;
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		for (int i = 0; i < (nums.length - 2); ++i) {
			int value = nums[i];
			if (target < value) {
				break;
			}
			int diff = target - value;
			
			int left = i + 1;
			int right = nums.length - 1;
			while ((left < right) && (nums[left] <= diff)) {
				int sum = nums[left] + nums[right];
				if (sum < diff) {
					++left;
				}
				else if (diff < sum) {
					--right;
				}
				else {
					results.add(Arrays.asList(value, nums[left], nums[right]));
					while ((left < right) && (nums[left] == nums[left + 1])) {
						++left;
					}
					++left;
					while ((left < right) && (nums[right] == nums[right - 1])) {
						--right;
					}
					--right;
				}
			}
			while ((value == nums[i + 1]) && ((i + 1) < (nums.length - 2))) {
				++i;
			}
		}
        return results;
    }
}
