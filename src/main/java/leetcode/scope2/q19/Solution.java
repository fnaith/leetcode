package leetcode.scope2.q19;

public class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
    	ListNode begin = head;
    	ListNode end = head;
    	for (int i = 0; i < n; ++i) {
    		end = end.next;
    	}
    	if (null == end) {
    		return head.next;
    	}
    	while (true) {
    		end = end.next;
    		if (null == end) {
    			break;
    		}
    		begin = begin.next;
    	}
    	if (null == begin.next) {
    		head = null;
    	}
    	else {
        	begin.next = begin.next.next;
    	}
        return head;
    }
    
    public ListNode arrayToList(int[] digits) {
    	ListNode head = new ListNode(digits[0]);
    	ListNode tail = head;
    	for (int i = 1; i < digits.length; ++i) {
    		ListNode node = new ListNode(digits[i]);
    		tail.next = node;
    		tail = node;
    	}
    	return head;
    }    
    
    public int[] listToArray(ListNode list) {
    	int[] digits = new int[getListLength(list)];
    	for (int i = 0; i < digits.length; ++i) {
    		digits[i] = list.val;
    		list = list.next;
    	}
    	return digits;
    }
    
    private int getListLength(ListNode list) {
    	int length = 0;
    	while (null != list) {
    		++length;
    		list = list.next;
    	}
    	return length;
    }

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
}
