package leetcode.scope2.q17;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {
	private static final char[][] LETTERSES = {
		{'a', 'b', 'c'},
		{'d', 'e', 'f'},
		{'g', 'h', 'i'},
		{'j', 'k', 'l'},
		{'m', 'n', 'o'},
		{'p', 'q', 'r', 's'},
		{'t', 'u', 'v'},
		{'w', 'x', 'y', 'z'}
	};
	
    public List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) {
            return new ArrayList<String>();
        }
    	int length = digits.length();
    	int count = 1;
    	int[] dims = new int[length];
    	char[] chars = new char[length];
    	for (int i = 0; i < length; ++i) {
    		char[] letters = LETTERSES[digitToIndex(digits.charAt(i))];
    		dims[i] = letters.length;
    		count *= letters.length;
    		chars[i] = letters[0];
    	}
    	List<String> results = new ArrayList<String>(count);
    	int[] indexes = new int[length];
    	indexes[0] = -1;
    	for (int i = 0; i < count; ++i) {
    		++indexes[0];
    		for (int j = 0; j < length; ++j) {
        		char[] letters = LETTERSES[digitToIndex(digits.charAt(j))];
    			if (dims[j] == indexes[j]) {
    				++indexes[j + 1];
    				indexes[j] = 0;
    				chars[j] = letters[0];
    			}
    			else {
    				chars[j] = letters[indexes[j]];
    				break;
    			}
    		}
    		results.add(String.valueOf(chars));
    	}
    	Collections.sort(results); // can be removed on leetcode
        return results;
    }
    
    private int digitToIndex(char ch) {
    	return (ch - '2');
    }
}
