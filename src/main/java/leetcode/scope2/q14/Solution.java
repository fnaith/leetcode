package leetcode.scope2.q14;

class Solution {
    public String longestCommonPrefix(String[] strs) {
    	switch (strs.length) {
    	case 0: return "";
    	case 1: return strs[0];
    	}
    	char[] chars = strs[0].toCharArray();
    	int sameLength = chars.length;
    	for (int i = 1; i < strs.length; ++i) {
    		String str = strs[i];
    		sameLength = Math.min(sameLength, str.length());
    		for (int j = 0; j < sameLength; ++j) {
    			if (chars[j] != str.charAt(j)) {
    				sameLength = j;
    				break;
    			}
    		}
    	}
        return String.valueOf(chars, 0, sameLength);
    }
}
