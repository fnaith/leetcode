package leetcode.scope2.q13;

class Solution {
    public int romanToInt(String s) {
    	int value = 0;
    	char lastSymbol = '\0';
    	for (int i = 0; i < s.length(); ++i) {
    		char symbol = s.charAt(i);
    		switch (symbol) {
    		case 'M': value += (('C' == lastSymbol) ? 800 : 1000); break;
    		case 'D': value += (('C' == lastSymbol) ? 300 : 500); break;
    		case 'C': value += (('X' == lastSymbol) ? 80 : 100); break;
    		case 'L': value += (('X' == lastSymbol) ? 30 : 50); break;
    		case 'X': value += (('I' == lastSymbol) ? 8 : 10); break;
    		case 'V': value += (('I' == lastSymbol) ? 3 : 5); break;
    		case 'I': value += 1; break;
    		}
    		lastSymbol = symbol;
    	}
        return value;
    }
}
