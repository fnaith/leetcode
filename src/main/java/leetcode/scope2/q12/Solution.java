package leetcode.scope2.q12;

class Solution {
	private static final int[] VALUES = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	private static final String[] SYMBOLS = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
	
    public String intToRoman(int num) {
    	StringBuilder digits = new StringBuilder();
    	for (int i = 0; i < VALUES.length; ++i) {
    		int value = VALUES[i];
    		if (value <= num) {
    			int times = num / value;
    			num -= times * value;
    			String symbol = SYMBOLS[i];
    			for (int time = 0; time < times; ++time) {
    				digits.append(symbol);
    			}
    		}
    	}
        return digits.toString();
    }
}
