package leetcode.scope2.q20;

public class Solution {
    public boolean isValid(String s) {
    	char[] chars = new char[s.length()];
    	int charIndex = -1;
    	for (int i = 0; i < s.length(); ++i) {
    		char ch = s.charAt(i);
    		switch (ch) {
    		case '(':
    		case '{':
    		case '[':
    			++charIndex;
    			chars[charIndex] = ch;
    			break;
    		case ')':
    			if ((charIndex < 0) || ('(' != chars[charIndex])) {
    				return false;
    			}
    			--charIndex;
    			break;
    		case '}':
    			if ((charIndex < 0) || ('{' != chars[charIndex])) {
    				return false;
    			}
    			--charIndex;
    			break;
    		case ']':
    			if ((charIndex < 0) || ('[' != chars[charIndex])) {
    				return false;
    			}
    			--charIndex;
    			break;
    		}
    	}
        return (-1 == charIndex);
    }
}
