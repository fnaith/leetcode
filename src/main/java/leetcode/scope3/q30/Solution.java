package leetcode.scope3.q30;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Solution {
	public List<Integer> findSubstring(String s, String[] words) {
		int wordCount = words.length;
		if (wordCount < 1) {
			return new ArrayList<Integer>();
		}
		int wordWidth = words[0].length();
		int resultWidthOffset = wordWidth * (wordCount - 1);
		Map<String, Integer> wordToCount = new HashMap<String, Integer>();
		for (String word : words) {
			int count = 1;
			if (wordToCount.containsKey(word)) {
				count += wordToCount.get(word);
			}
			wordToCount.put(word, count);
		}
		Queue<String> matchedWords = new LinkedList<String>();	
		Map<String, Integer> matchedWordCount = new HashMap<String, Integer>();		
		List<Integer> results = new ArrayList<Integer>();
		for (int w = 0; w < wordWidth; ++w) {
			int head = w;
			matchedWords.clear();
			matchedWordCount.clear();
			for (int charIndex = w; charIndex <= (s.length() - wordWidth); charIndex += wordWidth) {
				String word = s.substring(charIndex, charIndex + wordWidth);
				if (!wordToCount.containsKey(word)) {
					matchedWords.clear();
					matchedWordCount.clear();
					head = charIndex + wordWidth;
					continue;
				}
				int matchedCount = 1;
				if (matchedWordCount.containsKey(word)) {
					matchedCount += matchedWordCount.get(word);
				}
				matchedWords.add(word);
				matchedWordCount.put(word, matchedCount);
				if (wordToCount.get(word) < matchedCount) {
					while (!word.equals(matchedWords.peek())) {
						String otherWord = matchedWords.remove();
						matchedWordCount.put(otherWord, matchedWordCount.get(otherWord) - 1);
						head += wordWidth;
					}
					matchedWords.remove();
					matchedWordCount.put(word,  matchedCount - 1);
					head += wordWidth;
				}
				if (resultWidthOffset == (charIndex - head)) {
					results.add(head);
					String firstWord = matchedWords.remove();
					matchedWordCount.put(firstWord, matchedWordCount.get(firstWord) - 1);
					head += wordWidth;
				}
			}
		}
        return results;
    }
}
