package leetcode.scope3.q28;

import java.util.Arrays;

public class Solution {
	private static final int ALPHABET_SIZE = 256;
	
	public int strStr(String haystack, String needle) {
		if (needle.isEmpty()) {
			return 0;
		}
		char[] chars = haystack.toCharArray();
		char[] pattern = needle.toCharArray();
		int[] shiftes = bmhPreprocess(pattern);
		return bmhSearch(chars, pattern, shiftes);
    }
	
	private int[] bmhPreprocess(char[] pattern) {
		int[] shiftes = new int[ALPHABET_SIZE];
		Arrays.fill(shiftes, pattern.length);
		for (int i = 0; i < (pattern.length - 1); ++i) {
			shiftes[pattern[i]] = pattern.length - 1 - i;
		}
		return shiftes;
	}
	
	private int bmhSearch(char[] chars, char[] pattern, int[] shiftes) {
	    int charIndex = 0;
	    while (charIndex <= (chars.length - pattern.length)) {
		    int patternIndex = pattern.length - 1;
		    while (chars[charIndex + patternIndex] == pattern[patternIndex]) {
		    	--patternIndex;
		    	if (patternIndex < 0) {
		    		return charIndex;
		    	}
		    }
		    charIndex += shiftes[chars[charIndex + pattern.length - 1]];
	    }
	    return -1;
	}
}
