package leetcode.scope3.q24;

public class Solution {
    public ListNode swapPairs(ListNode head) {
    	if ((null == head) || (null == head.next)) {
    		return head;
    	}
    	ListNode result = head.next;
    	ListNode prevFirst = new ListNode(0);
    	prevFirst.next = head;
    	ListNode first = head;
    	ListNode second;
    	while (null != first) {
    		second = first.next;
    		if (null == second) {
    			break;
    		}
    		first.next = second.next;
    		second.next = first;
    		prevFirst.next = second;
    		prevFirst = first;
    		first = first.next;
    	}
    	return result;
    }
    
    public ListNode arrayToList(int[] digits) {
    	ListNode head = new ListNode(digits[0]);
    	ListNode tail = head;
    	for (int i = 1; i < digits.length; ++i) {
    		ListNode node = new ListNode(digits[i]);
    		tail.next = node;
    		tail = node;
    	}
    	return head;
    }
    
    public int[] listToArray(ListNode list) {
    	int[] digits = new int[getListLength(list)];
    	for (int i = 0; i < digits.length; ++i) {
    		digits[i] = list.val;
    		list = list.next;
    	}
    	return digits;
    }
    
    private int getListLength(ListNode list) {
    	int length = 0;
    	while (null != list) {
    		++length;
    		list = list.next;
    	}
    	return length;
    }

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
}
