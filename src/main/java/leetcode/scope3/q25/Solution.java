package leetcode.scope3.q25;

public class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
    	ListNode prevFirst = new ListNode(0);
    	prevFirst.next = head;
    	ListNode first = head;
    	ListNode result = null;
    	ListNode tmp;
    	while (null != first) {
    		for (int i = 1; i < k; ++i) {
    			if (null == first.next) {
    				first = prevFirst.next;
    	    		for (int j = 1; j < i; ++j) {
    	    			tmp = prevFirst.next;
    	    			prevFirst.next = first.next;
    	    			first.next = first.next.next;
    	    			prevFirst.next.next = tmp;
    	    		}
    				break;
    			}
    			tmp = prevFirst.next;
    			prevFirst.next = first.next;
    			first.next = first.next.next;
    			prevFirst.next.next = tmp;
    		}
    		if (null == result) {
    			result = prevFirst.next;
    		}
    		prevFirst = first;
    		first = first.next;
    	}
		if (null == result) {
			result = head;
		}
    	return result;
    }
    
    public ListNode arrayToList(int[] digits) {
    	ListNode head = new ListNode(digits[0]);
    	ListNode tail = head;
    	for (int i = 1; i < digits.length; ++i) {
    		ListNode node = new ListNode(digits[i]);
    		tail.next = node;
    		tail = node;
    	}
    	return head;
    }
    
    public int[] listToArray(ListNode list) {
    	int[] digits = new int[getListLength(list)];
    	for (int i = 0; i < digits.length; ++i) {
    		digits[i] = list.val;
    		list = list.next;
    	}
    	return digits;
    }
    
    private int getListLength(ListNode list) {
    	int length = 0;
    	while (null != list) {
    		++length;
    		list = list.next;
    	}
    	return length;
    }

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
}
