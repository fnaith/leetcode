package leetcode.scope3.q29;

public class Solution {
    public int divide(int dividend, int divisor) {
    	if (1 == divisor) {
    		return ((0 < dividend) ? dividend : ((Integer.MIN_VALUE == dividend) ? Integer.MIN_VALUE : dividend));
    	}
    	if (-1 == divisor) {
    		return ((0 < dividend) ? -dividend : ((Integer.MIN_VALUE == dividend) ? Integer.MAX_VALUE : -dividend));
    	}
    	if (Integer.MIN_VALUE == divisor) {
    		return ((Integer.MIN_VALUE == dividend) ? 1 : 0);
    	}
    	int div = 0;
    	if (Integer.MIN_VALUE == dividend) {
    		if (0 < divisor) {
    			dividend += divisor;
    			--div;
    		}
    		else {
    			dividend -= divisor;
    			++div;
    		}
    	}
    	boolean sameSign = true;
    	if (dividend < 0) {
    		dividend = -dividend;
    		sameSign = !sameSign;
        }
		if (divisor < 0) {
			divisor = -divisor;
			sameSign = !sameSign;
    	}
    	while (true) {
    		if (dividend < divisor) {
    			break;
    		}
    		int tmpDivisor = divisor;
    		int tmpDiv = 1;
    		while ((0 < (tmpDivisor << 1)) && ((tmpDivisor << 1) < dividend)) {
    			tmpDivisor <<= 1;
    			tmpDiv <<= 1;
    		}
    		dividend -= tmpDivisor;
    		if (sameSign) {
    			div += tmpDiv;
    		}
    		else {
    			div -= tmpDiv;
    		}
    	}
        return div;
    }
}
