package leetcode.scope3.q22;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Solution {
    public List<String> generateParenthesis(int n) {
    	int limit = n * 2;
    	List<String> results = new ArrayList<String>();
    	Queue<char[]> buffers = new LinkedList<char[]>();
    	Queue<Integer> leftCounts = new LinkedList<Integer>();
    	Queue<Integer> rightCounts = new LinkedList<Integer>();
    	buffers.add(new char[limit]);
    	leftCounts.add(0);
    	rightCounts.add(0);
    	while (!buffers.isEmpty()) {
    		char[] buffer = buffers.remove();
    		int leftCount = leftCounts.remove();
    		int rightCount = rightCounts.remove();
    		int size = leftCount + rightCount;
    		if (limit == size) {
    			results.add(String.valueOf(buffer));
    		}
    		else {
    			boolean pushed = false;
    			if (n != leftCount) {
    				buffer[size] = '(';
    				buffers.add(buffer);
    				leftCounts.add(leftCount + 1);
    				rightCounts.add(rightCount);
    				pushed = true;
    			}
    			if (rightCount < leftCount) {
    				if (pushed) {
    					buffer = buffer.clone();
    				}
    				buffer[size] = ')';
    				buffers.add(buffer);
    				leftCounts.add(leftCount);
    				rightCounts.add(rightCount + 1);
    			}
    		}
    	}
        return results;
    }
}
