package leetcode.scope3.q26;

public class Solution {
    public int removeDuplicates(int[] nums) {
    	int lastIndex = 0;
    	for (int i = 1; i < nums.length; ++i) {
    		if (nums[lastIndex] == nums[i]) {
    			continue;
    		}
    		++lastIndex;
    		nums[lastIndex] = nums[i];
    	}
        return lastIndex + 1;
    }
}
