package leetcode.scope3.q23;

public class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
    	if (0 == lists.length) {
    		return null;
    	}
    	
    	int half = 1;
    	int step = 2;
    	while (half < lists.length) {
        	for (int i = 0; i < lists.length; i += step) {
        		if ((i + half) < lists.length) {
            		lists[i] = mergeTwoLists(lists[i], lists[i + half]);
        		}
        	}
        	half = step;
        	step *= 2;
    	}
        return lists[0];
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
    	ListNode head = new ListNode(0);
    	ListNode tail = head;
    	while (true) {
    		if (null == l1) {
    			tail.next = l2;
    			break;
    		}
    		if (null == l2) {
    			tail.next = l1;
    			break;
    		}
        	if (l1.val < l2.val) {
        		tail.next = l1;
        		l1 = l1.next;
        	}
        	else {
        		tail.next = l2;
        		l2 = l2.next;
        	}
        	tail = tail.next;
    	}
        return head.next;
    }
    
    public ListNode arrayToList(int[] digits) {
    	ListNode head = new ListNode(digits[0]);
    	ListNode tail = head;
    	for (int i = 1; i < digits.length; ++i) {
    		ListNode node = new ListNode(digits[i]);
    		tail.next = node;
    		tail = node;
    	}
    	return head;
    }    
    
    public int[] listToArray(ListNode list) {
    	int[] digits = new int[getListLength(list)];
    	for (int i = 0; i < digits.length; ++i) {
    		digits[i] = list.val;
    		list = list.next;
    	}
    	return digits;
    }
    
    private int getListLength(ListNode list) {
    	int length = 0;
    	while (null != list) {
    		++length;
    		list = list.next;
    	}
    	return length;
    }

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
}
