package leetcode.scope3.q27;

public class Solution {
    public int removeElement(int[] nums, int val) {
    	int lastIndex = -1;
    	for (int i = 0; i < nums.length; ++i) {
    		if (val == nums[i]) {
    			continue;
    		}
    		++lastIndex;
    		nums[lastIndex] = nums[i];
    	}
        return lastIndex + 1;
    }
}
