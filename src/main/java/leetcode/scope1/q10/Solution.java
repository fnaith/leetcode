package leetcode.scope1.q10;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public boolean isMatch(String s, String p) {
    	List<Pattern> patterns = new ArrayList<Pattern>();
    	for (int i = 0; i < p.length(); ++i) {
    		char pattern = p.charAt(i);
    		boolean anyTimes = false;
    		if ((i + 1) < p.length()) {
    			anyTimes = isAnyTimes(p.charAt(i + 1));
        		if (anyTimes) {
        			++i;
        		}
    		}
    		patterns.add(new Pattern(pattern, anyTimes));
    	}
    	
    	boolean[][] dp = new boolean[patterns.size() + 1][];
    	dp[0] = new boolean[s.length() + 1];
    	dp[0][0] = true;
    	for (int i = 0; i < patterns.size(); ++i) {
    		dp[i + 1] = new boolean[s.length() + 1];
    		dp[i + 1][0] = dp[i][0] && patterns.get(i).anyTimes;
    	}
    	
    	for (int i = 0; i < patterns.size(); ++i) {
        	for (int j = 0; j < s.length(); ++j) {
    			Pattern pattern = patterns.get(i);
    			char ch = s.charAt(j);
    			if (pattern.anyTimes) {
    				dp[i + 1][j + 1] = dp[i][j + 1] || (dp[i + 1][j] && pattern.matchSingle(ch));
    			}
    			else {
    				dp[i + 1][j + 1] = dp[i][j] && pattern.matchSingle(ch);
    			}
        	}
    	}
    	
    	return dp[patterns.size()][s.length()];
    }
    
    public class Pattern {
    	char pattern;
    	boolean anyTimes;
    	
    	Pattern(char pattern, boolean anyTimes) {
    		this.pattern = pattern;
    		this.anyTimes = anyTimes;
    	}
    	
    	boolean matchSingle(char ch) {
    		return ((pattern == ch) || ('.' == pattern));
    	}
    }
	
	private boolean isAnyTimes(char ch) {
		return ('*' == ch);
	}
}
