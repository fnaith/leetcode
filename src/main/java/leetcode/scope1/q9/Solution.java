package leetcode.scope1.q9;

public class Solution {
	private static final int BASE = 10;
	
	public boolean isPalindrome(int x) {
		if (x < 0) {
			return false;
		}
    	int[] digits = new int[32];
    	int index = 0;
    	while (0 < x) {
    		digits[index] = x % BASE;
    		++index;
    		x /= BASE;
    	}
    	for (int i = 0 ; i < index / 2; ++i) {
    		if (digits[i] != digits[index - 1 - i]) {
    			return false;
    		}
    	}
		return true;
	}
}
