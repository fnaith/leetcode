package leetcode.scope1.q5;

class Solution {
	private static final char TOKEN = '#';
	
    public String longestPalindrome(String s) {
    	char[] chars = toManacherString(s);
    	
    	int[] palindromeRadiuses = new int[chars.length];
    	
    	int rightBound = 0;
    	int centerOfRightBound = 0;
    	int maxPalindromeRadius = 0;
    	int maxPalindromeCenter = 0;
    	
    	for (int i = 0; i < chars.length; ++i) {
    		int mirrorOfCenter = centerOfRightBound * 2 - i;
    		int leastPalindromeRadius = ((i < rightBound) ? Math.min(palindromeRadiuses[mirrorOfCenter], rightBound - i) : 1);
    		palindromeRadiuses[i] = findPalindromeRadius(i, chars, leastPalindromeRadius);
    		if (rightBound < i + palindromeRadiuses[i]) {
    			rightBound = i + palindromeRadiuses[i];
    			centerOfRightBound = i;
    		}
    		if (maxPalindromeRadius < palindromeRadiuses[i]) {
    			maxPalindromeRadius = palindromeRadiuses[i];
    			maxPalindromeCenter = i;
    		}
    	}
    	
        return fromManacherString(chars, maxPalindromeCenter, maxPalindromeRadius);
    }
    
    private char[] toManacherString(String str) {
        char[] chars = str.toCharArray();
        char[] res = new char[chars.length * 2 + 1];
        for (int i = 0; i < chars.length; ++i) {
            res[i * 2] = TOKEN;
            res[i * 2 + 1] = chars[i];
        }
        res[res.length - 1] = TOKEN;
        return res;
    }
    
    private int findPalindromeRadius(int center, char[] chars, int leastPalindromeRadius) {
    	int palindromeRadius = leastPalindromeRadius;
    	while ((0 <= (center - palindromeRadius)) && ((center + palindromeRadius) < chars.length) &&
    		(chars[center - palindromeRadius] == chars[center + palindromeRadius])) {
    		++palindromeRadius;
    	}
    	return palindromeRadius;
    }
    
    private String fromManacherString(char[] chars, int center, int radius) {
    	char[] res = new char[radius - 1];
    	for (int i = 0; i < res.length; ++i) {
    		res[i] = chars[center - radius + 2 + i * 2];
    	}
    	return String.valueOf(res);
    }
}
