package leetcode.scope1.q3;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public int lengthOfLongestSubstring(String s) {
    	if (s.isEmpty()) {
    		return 0;
    	}
    	int left = 0;
    	int maxLength = 0;
    	char[] chars = s.toCharArray();
    	Map<Character, Integer> charToIndex = new HashMap<Character, Integer>();
    	charToIndex.put(chars[0], 0);
    	for (int right = 1; right < chars.length; ++right) {
    		char ch = chars[right];
    		if (charToIndex.containsKey(ch)) {
    			left = Math.max(left, charToIndex.get(ch) + 1);
    		}
    		maxLength = Math.max(maxLength, right - left);
        	charToIndex.put(ch, right);
    	}
        return maxLength + 1;
    }
}
