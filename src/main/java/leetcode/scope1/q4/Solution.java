package leetcode.scope1.q4;

class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
    	int length = nums1.length + nums2.length;
    	if (0 == (length & 1)) {
    		int index1 = length / 2;
    		int index2 = index1 + 1;
    		return (findKthValue(index1, nums1, nums2) + findKthValue(index2, nums1, nums2)) / 2.0;
    	}
    	else {
    		int index = (length + 1) / 2;
    		return findKthValue(index, nums1, nums2);
    	}
    }
    
    private int findKthValue(int k, int[] nums1, int[] nums2) {
    	int head1 = 0;
    	int head2 = 0;
    	while (true) {
    		if (nums1.length == head1) {
    			return nums2[head2 + k - 1];
    		}
    		if (nums2.length == head2) {
    			return nums1[head1 + k - 1];
    		}
    		if (1 == k) {
    			return Math.min(nums1[head1], nums2[head2]);
    		}
    		int half = k / 2;
    		int newHead1 = Math.min(head1 + half - 1, nums1.length - 1);
    		int newHead2 = Math.min(head2 + half - 1, nums2.length - 1);
    		int val1 = nums1[newHead1];
    		int val2 = nums2[newHead2];
    		if (val1 < val2) {
    			k -= (newHead1 + 1 - head1);
    			head1 = newHead1 + 1;
    		}
    		else {
    			k -= (newHead2 + 1 - head2);
    			head2 = newHead2 + 1;
    		}
    	}
    }
}
