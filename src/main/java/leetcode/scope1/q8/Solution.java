package leetcode.scope1.q8;

public class Solution {
	private static final int BASE = 10;
	private static final int MAX = 2147483647;
	
    public int myAtoi(String str) {
    	if (str.isEmpty()) {
    		return 0;
    	}
    	int firstNonSpaceIndex = 0;
    	for (int i = 0; i < str.length(); ++i) {
    		if (!isSpace(str.charAt(i))) {
    			firstNonSpaceIndex = i;
    			break;
    		}
    	}
    	int startDigitIndex = firstNonSpaceIndex;
    	boolean neg = false;
    	if ('+' == str.charAt(firstNonSpaceIndex)) {
    		++startDigitIndex;
    	}
    	else if ('-' == str.charAt(firstNonSpaceIndex)) {
    		++startDigitIndex;
    		neg = true;
    	}
    	int x = 0;
    	for (int i = startDigitIndex; i < str.length(); ++i) {
    		char ch = str.charAt(i);
    		if (!isDigit(ch)) {
    			break;
    		}
    		int digit = toDigit(ch);
    		if (((MAX - digit) / BASE) < x) {
    			if (neg) {
    				return -MAX - 1;
    			}
    			else {
    				return MAX;
    			}
    		}
    		x = x * BASE + digit;
    		++startDigitIndex;
    	}
    	if (neg) {
    		x = -x;
    	}
        return x;
    }
	
	private boolean isSpace(char ch) {
		return (' ' == ch);
	}
	
	private boolean isDigit(char ch) {
		return (('0' <= ch) && (ch <= '9'));
	}
	
	private int toDigit(char ch) {
		return (ch - '0');
	}
}
