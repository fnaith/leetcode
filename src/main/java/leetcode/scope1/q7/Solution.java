package leetcode.scope1.q7;

public class Solution {
	private static final int BASE = 10;
	private static final int MAX = 2147483647;
	
    public int reverse(int x) {
    	boolean neg = false;
    	if (x < 0) {
    		neg = true;
    		x = -x;
    		if (x < 0) {
    			return 0;
    		}
    	}
    	int[] digits = new int[32];
    	int index = 0;
    	while (0 < x) {
    		digits[index] = x % BASE;
    		++index;
    		x /= BASE;
    	}
    	for (int i = 0 ; i < index; ++i) {
    		if ((MAX - digits[i]) / BASE < x) {
    			return 0;
    		}
    		x = x * BASE + digits[i];
    	}
    	if (neg) {
    		x = -x;
    	}
        return x;
    }
}
