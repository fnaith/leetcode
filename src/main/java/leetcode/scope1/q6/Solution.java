package leetcode.scope1.q6;

public class Solution {
    public String convert(String s, int numRows) {
    	if (1 == numRows) {
    		return s;
    	}
    	char[] chars = new char[s.length()];
    	int period = (numRows - 1) * 2;
    	int charIndex = 0;
    	for (int row = 0; row < numRows; ++row) {
    		for (int i = row; i < chars.length; i += period) {
    			chars[charIndex] = s.charAt(i);
    			++charIndex;
    			if ((0 == row) || ((numRows - 1) == row)) {
    				continue;
    			}
    			int j = i + (numRows - 1 - row) * 2;
    			if (j < chars.length) {
        			chars[charIndex] = s.charAt(j);
        			++charIndex;
    			}
    		}
    	}
        return String.valueOf(chars);
    }
}
