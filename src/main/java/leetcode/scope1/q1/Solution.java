package leetcode.scope1.q1;

import java.util.HashMap;
import java.util.Map;

public class Solution {
	public int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> valueToIndex = new HashMap<Integer, Integer>();
		valueToIndex.put(nums[0], 0);
		for (int i = 1; i < nums.length; ++i) {
			int value = nums[i];
			int diff = target - value;
			if (valueToIndex.containsKey(diff)) {
				return new int[] {valueToIndex.get(diff), i};
			}
            else {
                valueToIndex.put(value, i);
            }
		}
		return null;
	}
}
