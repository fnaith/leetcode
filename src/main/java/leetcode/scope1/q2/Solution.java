package leetcode.scope1.q2;

public class Solution {
	private static final int BASE = 10;
	
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    	ListNode l3 = new ListNode(0);
    	ListNode node = l3;
    	while (true) {
    		if (null != l1) {
    			node.val += l1.val;
        		l1 = l1.next;
    		}
    		if (null != l2) {
    			node.val += l2.val;
        		l2 = l2.next;
    		}
    		
    		boolean noCarry = true;
    		if (BASE <= node.val) {
    			node.val -= BASE;
    			pushValue(node, 1);
    			node = node.next;
    			noCarry = false;
    		}

    		if ((null == l1) && (null == l2)) {
    			break;
    		}
    		
    		if (noCarry) {
    			pushValue(node, 0);
    			node = node.next;
    		}
    	}
        return l3;
    }
    
    public ListNode arrayToList(int[] digits) {
    	ListNode head = new ListNode(digits[0]);
    	for (int i = 1; i < digits.length; ++i) {
    		ListNode node = new ListNode(digits[i]);
    		node.next = head;
    		head = node;
    	}
    	return head;
    }    
    
    public int[] listToArray(ListNode list) {
    	int[] digits = new int[getListLength(list)];
    	for (int i = 0; i < digits.length; ++i) {
    		digits[digits.length - 1 - i] = list.val;
    		list = list.next;
    	}
    	return digits;
    }
    
    private int getListLength(ListNode list) {
    	int length = 0;
    	while (null != list) {
    		++length;
    		list = list.next;
    	}
    	return length;
    }
    
    private void pushValue(ListNode list, int x) {
		list.next = new ListNode(x);
    }

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
}
