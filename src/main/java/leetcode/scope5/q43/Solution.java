package leetcode.scope5.q43;

public class Solution {
	private static final int BASE = 10;
	
    public String multiply(String num1, String num2) {
    	int[] digits1 = new int[num1.length()];
    	int[] digits2 = new int[num2.length()];
    	for (int i = 0; i < num1.length(); ++i) {
    		digits1[i] = num1.charAt(num1.length() - 1 - i) - '0';
    	}
    	for (int i = 0; i < num2.length(); ++i) {
    		digits2[i] = num2.charAt(num2.length() - 1 - i) - '0';
    	}
    	int[] digits3 = new int[digits1.length + digits2.length];
    	for (int i = 0; i < digits1.length; ++i) {
        	for (int j = 0; j < digits2.length; ++j) {
        		digits3[i + j] += digits1[i] * digits2[j];
        	}
    	}
    	for (int i = 0; i < digits3.length - 1; ++i) {
    		digits3[i + 1] += (digits3[i] / BASE);
    		digits3[i] %= BASE;
    	}
    	StringBuilder sb = new StringBuilder();
    	boolean foundDigit = false;
    	for (int i = digits3.length - 1; 0 <= i; --i) {
    		if (!foundDigit && (0 != digits3[i])) {
    			foundDigit = true;
    		}
    		if (foundDigit) {
    			sb.append((char)(digits3[i] + '0'));
    		}
    	}
        String result = sb.toString();
        if (result.isEmpty()) {
        	result = "0";
        }
        return result;
    }
}
