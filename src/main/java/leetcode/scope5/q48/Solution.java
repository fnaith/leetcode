package leetcode.scope5.q48;

public class Solution {
    public void rotate(int[][] matrix) {
        int n = matrix.length;
        int layers = n / 2;
        for (int layer = 0; layer < layers; ++layer) {
            int size = n - 2 * layer;
            int steps = size - 1;
            for (int step = 0; step < steps; ++step) {
                rotate4Value(matrix, layer, size, step);
            }
        }
    }

    private void rotate4Value(int[][] matrix, int layer, int size, int step) {
        int x0 = layer + step;
        int y0 = layer + 0;
        int x1 = layer + size - 1;
        int y1 = layer + step;
        int x2 = layer + size - 1 - step;
        int y2 = layer + size - 1;
        int x3 = layer + 0;
        int y3 = layer + size - 1 - step;
        int v0 = matrix[y0][x0];
        int v1 = matrix[y1][x1];
        int v2 = matrix[y2][x2];
        int v3 = matrix[y3][x3];
        matrix[y0][x0] = v3;
        matrix[y1][x1] = v0;
        matrix[y2][x2] = v1;
        matrix[y3][x3] = v2;
    }
}
