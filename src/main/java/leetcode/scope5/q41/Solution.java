package leetcode.scope5.q41;

public class Solution {
	public int firstMissingPositive(int[] nums) {
		for (int i = 0; i < nums.length; ++i) {
			int num = nums[i];
			while ((0 < num) && (num <= nums.length) && ((i + 1) != num)) {
				if (num == nums[num - 1]) {
					break;
				}
				nums[i] = nums[num - 1];
				nums[num - 1] = num;
				num = nums[i];
			}
		}
		for (int i = 0; i < nums.length; ++i) {
			if ((i + 1) != nums[i]) {
				return (i + 1);
			}
		}
        return (nums.length + 1);
    }
}
