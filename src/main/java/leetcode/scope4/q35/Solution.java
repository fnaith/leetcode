package leetcode.scope4.q35;

public class Solution {
    public int searchInsert(int[] nums, int target) {
    	int left = 0;
    	int right = nums.length - 1;
    	int index = -1;
    	while (left <= right) {
    		int middle = (left + right) / 2;
    		if (nums[middle] < target) {
    			left = middle + 1;
    			index = left;
    		}
    		else if (target < nums[middle]) {
    			right = middle - 1;
    			index = left;
    		}
    		else {
    			index = middle;
    			break;
    		}
    	}
        return index;
    }
}
