package leetcode.scope4.q37;

public class Solution {
	private static final int SIZE = 9;
	private static final int SUB_SIZE = 3;
	private static final int DIGIT_NUMBER = 81;
	
    public void solveSudoku(char[][] board) {
    	int[] rows = new int[SIZE];
    	int[] columns = new int[SIZE];
    	int[] boxes = new int[SIZE];
    	int[] digitIndexes = new int[DIGIT_NUMBER];
    	int digits = 0;
    	for (int i = 0; i < DIGIT_NUMBER; ++i) {
    		int row = i / SIZE;
    		int column = i % SIZE;
			int box = (row / SUB_SIZE) * SUB_SIZE + column / SUB_SIZE;
    		char ch = board[row][column];
    		if ('.' == ch) {
    			digitIndexes[digits] = (box << 16) | (column << 8) | row;
    			++digits;
    		}
    		else {
    			int digit = ch - '1';
    			int mask = 0x01 << digit;
    			rows[row] |= mask;
    			columns[column] |= mask;
    			boxes[box] |= mask;
    		}
    	}
    	int maxDigit = digits;
        while (0 < digits) {
        	--digits;
        	int digitIndex = digitIndexes[digits];
    		int row = digitIndex & 0xff;
    		digitIndex >>= 8;
    		int column = digitIndex & 0xff;
    		digitIndex >>= 8;
			int box = digitIndex & 0xff;
    		char ch = board[row][column];
    		if ('.' != ch) {
    			int digit = ch - '1';
    			int mask = 0x01 << digit;
    			rows[row] &= ~mask;
    			columns[column] &= ~mask;
    			boxes[box] &= ~mask;
    			board[row][column] = '.';
    		}
    		if ('.' == ch) {
    			ch = '1';
    		}
    		else {
    			++ch;
    		}
    		char nextCh = '.';
    		int currentMask = rows[row] | columns[column] | boxes[box];
    		for (char i = ch; i <= '9'; ++i) {
    			int mask = 0x01 << (i - '1');
    			if (0 != (currentMask & mask)) {
        			continue;
    			}
    			else {
        			nextCh = i;
    				break;
    			}
    		}
    		if ('.' == nextCh) {
    			digits += 2;
    			if (maxDigit < digits) {
    				return;
    			}
    			else {
    				continue;
    			}
    		}
			int mask = 0x01 << (nextCh - '1');
			rows[row] |= mask;
			columns[column] |= mask;
			boxes[box] |= mask;
    		board[row][column] = nextCh;
        }
    }
}
