package leetcode.scope4.q31;

public class Solution {
	public void nextPermutation(int[] nums) {
		testNextPermutation(nums);
    }
	
	public int[] testNextPermutation(int[] nums) {
		for (int i = nums.length - 1; 0 < i; --i) {
			int value = nums[i - 1];
			if (value < nums[i]) {
				for (int j = nums.length - 1; i <= j; --j) {
					if (value < nums[j]) {
						swap(nums, i - 1, j);
						reverseOrder(nums, i, nums.length - 1);
				        return nums;
					}
				}
			}
		}
		reverseOrder(nums, 0, nums.length - 1);
        return nums;
    }
	
	private void reverseOrder(int[] nums, int left, int right) {
		while (left < right) {
			swap(nums, left, right);
			++left;
			--right;
		}
	}
	
	private void swap(int[] nums, int i, int j) {
		int tmp = nums[i];
		nums[i] = nums[j];
		nums[j] = tmp;
	}
}
