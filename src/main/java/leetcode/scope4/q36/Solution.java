package leetcode.scope4.q36;

public class Solution {
	private static final int SIZE = 9;
	private static final int SUB_SIZE = 3;
	
    public boolean isValidSudoku(char[][] board) {
    	int[] rows = new int[SIZE];
    	int[] columns = new int[SIZE];
    	int[] boxes = new int[SIZE];
    	for (int row = 0; row < SIZE; ++row) {
        	for (int column = 0; column < SIZE; ++column) {
        		char ch = board[row][column];
        		if ('.' != ch) {
        			int digit = ch - '1';
        			int mask = 0x01 << digit;
        			int box = (row / SUB_SIZE) * SUB_SIZE + column / SUB_SIZE;
        			if ((0 != (rows[row] & mask)) || (0 != (columns[column] & mask)) || (0 != (boxes[box] & mask))) {
        		        return false;
        			}
        			rows[row] |= mask;
        			columns[column] |= mask;
        			boxes[box] |= mask;
        		}
        	}
    	}
        return true;
    }
}
