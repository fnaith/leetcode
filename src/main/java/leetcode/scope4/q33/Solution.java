package leetcode.scope4.q33;

public class Solution {
    public int search(int[] nums, int target) {
    	switch (nums.length) {
    	case 0: return -1;
    	case 1: return ((target == nums[0]) ? 0 : -1);
    	}
        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
        	int middle = (left + right) / 2;
        	if (target == nums[middle]) {
        		return middle;
        	}
        	if (nums[left] <= nums[middle]) {
        		if ((nums[left] <= target) && (target < nums[middle])) {
        			right = middle - 1;
        		}
        		else {
        			left = middle + 1;
        		}
        	}
        	else {
        		if ((nums[middle] < target) && (target <= nums[right])) {
        			left = middle + 1;
        		}
        		else {
        			right = middle - 1;
        		}
        	}
        }
        return ((target == nums[left]) ? left : -1);
    }
}
