package leetcode.scope4.q40;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Solution {
	public List<List<Integer>> combinationSum2(int[] candidates, int target) {
		if (0 == candidates.length) {
			return new ArrayList<List<Integer>>();
		}
		Map<Integer, Integer> candidateToCount = new HashMap<Integer, Integer>();
		for (int candidate : candidates) {
			int count = 1;
			if (candidateToCount.containsKey(candidate)) {
				count += candidateToCount.get(candidate);
			}
			candidateToCount.put(candidate, count);
		}
		Integer[] uniqueCandidates = new Integer[candidateToCount.size()];
		uniqueCandidates = candidateToCount.keySet().toArray(uniqueCandidates);
		Arrays.sort(uniqueCandidates);
		int left = 0;
		int right = uniqueCandidates.length - 1;
		while (left < right) {
			int tmp = uniqueCandidates[left];
			uniqueCandidates[left] = uniqueCandidates[right];
			uniqueCandidates[right] = tmp;
			++left;
			--right;
		}
		int length = uniqueCandidates.length;
		int[] candidateCounts = new int[length];
		Arrays.fill(candidateCounts, -1);
		ArrayList<List<Integer>> results = new ArrayList<List<Integer>>();
		Stack<Integer> result = new Stack<Integer>();
		int stackIndex = -1;
		int sum = 0;
		while (true) {
			++stackIndex;
			if (length <= stackIndex) {
				--stackIndex;
			}
			int candidate = uniqueCandidates[stackIndex];
			++candidateCounts[stackIndex];
			if (0 < candidateCounts[stackIndex]) {
				sum += candidate;
				result.push(candidate);
			}
			if ((candidateToCount.get(candidate) < candidateCounts[stackIndex]) || (target <= sum)) {
				if ((target == sum) && (candidateCounts[stackIndex] <= candidateToCount.get(candidate))) {
					results.add(new ArrayList<Integer>(result));
				}
				sum -= (candidateCounts[stackIndex] * candidate);
				for (int i = 0; i < candidateCounts[stackIndex]; ++i) {
					result.pop();
				}
				candidateCounts[stackIndex] = -1;
				--stackIndex;
				--stackIndex;
				if (-2 == stackIndex) {
					break;
				}
			}
		}
        return results;
    }
}
