package leetcode.scope4.q38;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {
    public String countAndSay(int n) {
    	--n;
    	if (n < 0) {
    		return "";
    	}
        Queue<Integer> values = new LinkedList<Integer>();
        values.add(1);
        for (int i = 0; i < n; ++i) {
        	int currentValue = values.remove();
        	int currentCount = 1;
            for (int j = values.size() - 1; 0 <= j; --j) {
        		int value = values.remove();
        		if (currentValue == value) {
        			++currentCount;
        		}
        		else {
        			values.add(currentCount);
        			values.add(currentValue);
        			currentValue = value;
        			currentCount = 1;
        		}
        	}
            values.add(currentCount);
            values.add(currentValue);
        }
        StringBuilder sb = new StringBuilder();
        for (Integer value : values) {
        	sb.append((char)('0' + value));
        }
        return sb.toString();
    }
}
