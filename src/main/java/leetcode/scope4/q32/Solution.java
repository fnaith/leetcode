package leetcode.scope4.q32;

public class Solution {
	public int longestValidParentheses(String s) {
		int max = 0;
		int left = 0;
		int right = 0;
		for (int i = 0; i < s.length(); ++i) {
			if ('(' == s.charAt(i)) {
				++left;
			}
			else {
				++right;
				if (left < right) {
					left = 0;
					right = 0;
				}
				else if (left == right) {
					max = Math.max(max, right);
				}
			}
		}
		left = 0;
		right = 0;
		for (int i = s.length() - 1; 0 <= i; --i) {
			if ('(' == s.charAt(i)) {
				++left;
				if (right < left) {
					left = 0;
					right = 0;
				}
				else if (left == right) {
					max = Math.max(max, left);
				}
			}
			else {
				++right;
			}
		}
        return max * 2;
    }
}
