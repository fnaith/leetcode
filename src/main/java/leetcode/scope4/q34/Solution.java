package leetcode.scope4.q34;

public class Solution {
    public int[] searchRange(int[] nums, int target) {
    	int begin = nums.length - 1;
    	int left = 0;
    	int right = nums.length - 1;
    	while (left <= right) {
    		int middle = (left + right) / 2;
    		if (target < nums[middle]) {
    			right = middle - 1;
    		}
    		else if (nums[middle] < target) {
    			left = middle + 1;
    		}
    		else {
    			begin = Math.min(middle, begin);
    			right = middle - 1;
    		}
    	}
    	int end = -1;
    	left = 0;
    	right = nums.length - 1;
    	while (left <= right) {
    		int middle = (left + right) / 2;
    		if (target < nums[middle]) {
    			right = middle - 1;
    		}
    		else if (nums[middle] < target) {
    			left = middle + 1;
    		}
    		else {
    			end = Math.max(middle, end);
    			left = middle + 1;
    		}
    	}
    	if (-1 == end) {
    		begin = -1;
    	}
        return new int[]{begin, end};
    }
}
