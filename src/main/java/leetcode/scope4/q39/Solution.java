package leetcode.scope4.q39;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Solution {
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		if (0 == candidates.length) {
			return new ArrayList<List<Integer>>();
		}
		Arrays.sort(candidates);
		int left = 0;
		int right = candidates.length - 1;
		while (left < right) {
			int tmp = candidates[left];
			candidates[left] = candidates[right];
			candidates[right] = tmp;
			++left;
			--right;
		}
		int length = candidates.length;
		int[] candidateCounts = new int[length];
		Arrays.fill(candidateCounts, -1);
		ArrayList<List<Integer>> results = new ArrayList<List<Integer>>();
		Stack<Integer> result = new Stack<Integer>();
		int stackIndex = -1;
		int sum = 0;
		while (true) {
			++stackIndex;
			if (length <= stackIndex) {
				--stackIndex;
			}
			int candidate = candidates[stackIndex];
			++candidateCounts[stackIndex];
			if (0 < candidateCounts[stackIndex]) {
				sum += candidate;
				result.push(candidate);
			}
			if (target <= sum) {
				if (target == sum) {
					results.add(new ArrayList<Integer>(result));
				}
				sum -= (candidateCounts[stackIndex] * candidate);
				for (int i = 0; i < candidateCounts[stackIndex]; ++i) {
					result.pop();
				}
				candidateCounts[stackIndex] = -1;
				--stackIndex;
				--stackIndex;
				if (-2 == stackIndex) {
					break;
				}
			}
		}
        return results;
    }
}
