package leetcode.week154;

import java.util.Stack;

public class Solution {
    public int maxNumberOfBalloons(String text) {
    	int b = 0;
    	int a = 0;
    	int l = 0;
    	int o = 0;
    	int n = 0;
    	for (int i = 0; i < text.length(); ++i) {
    		switch (text.charAt(i)) {
    		case 'b': ++b; break;
    		case 'a': ++a; break;
    		case 'l': ++l; break;
    		case 'o': ++o; break;
    		case 'n': ++n; break;
    		}
    	}
    	l /= 2;
    	o /= 2;
        return Math.min(Math.min(b, a), Math.min(Math.min(l, o), n));
    }
    
    public String reverseParentheses(String s) {
    	char[] chars = s.toCharArray();
    	Stack<Integer> leftINdexes = new Stack<Integer>();
    	for (int i = 0; i < s.length(); ++i) {
    		char ch = chars[i];
    		switch (ch) {
    		case '(':
    			leftINdexes.push(i);
    			break;
    		case ')':
    	    	int left = leftINdexes.pop() + 1;
    	    	int right = i - 1;
    			while (left < right) {
    				char tmpChar = chars[left];
    				chars[left] = chars[right];
    				chars[right] = tmpChar;
    				++left;
    				--right;
    			}
    			break;
    		}
    	}
    	StringBuilder sb = new StringBuilder();
    	for (char ch : chars) {
    		switch (ch) {
    		case '(': break;
    		case ')': break;
    		default: sb.append(ch);
    		}
    	}
        return sb.toString();
    }
    
    public int kConcatenationMaxSum(int[] arr, int k) {
    	int mod = 1000000007;
		int subSum = 0;
    	for (int value : arr) {
    		subSum = (subSum + (value % mod)) % mod;
    	}

		int maxSum = 0;
		int sum1 = 0;
    	for (int value : arr) {
    		if (sum1 <= 0) {
    			sum1 = value;
    		}
    		else {
    			sum1 += value;
    		}
    		maxSum = Math.max(maxSum, sum1);
    	}
    	
    	int sum2 = 0;
    	if (0 < subSum) {
    		for (int i = 2; i < k; ++i) {
    			sum2 = (sum2 + subSum) % mod;
    		}
    	}
		long rightSum = sum2;
		long rightMaxSum = 0;
		for (int i = 0; i < arr.length; ++i) {
			rightSum += arr[i];
			rightMaxSum = Math.max(rightMaxSum, rightSum);
		}
		sum2 = (int)(rightMaxSum % mod);
		long leftSum = sum2;
		long leftMaxSum = 0;
		for (int i = arr.length - 1; 0 <= i; --i) {
			leftSum += arr[i];
			leftMaxSum = Math.max(leftMaxSum, leftSum);
		}
		sum2 = (int)(leftMaxSum % mod);
		return Math.max(maxSum, sum2);
    }
}
