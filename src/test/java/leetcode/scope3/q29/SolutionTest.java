package leetcode.scope3.q29;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(3, solution.divide(10, 3));
    	assertEquals(-2, solution.divide(7, -3));
    	assertEquals(4, solution.divide(12, 3));
    	assertEquals(-2, solution.divide(6, -3));
    	assertEquals(-1, solution.divide(-1, 1));
    	assertEquals(2147483647, solution.divide(-2147483648, -1));
    	assertEquals(-2147483648, solution.divide(-2147483648, 1));
    	assertEquals(-1073741824, solution.divide(-2147483648, 2));
    	assertEquals(1, solution.divide(-2147483648, -2147483648));
    }
}
