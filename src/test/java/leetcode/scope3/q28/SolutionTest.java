package leetcode.scope3.q28;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(2, solution.strStr("hello", "ll"));
    	assertEquals(-1, solution.strStr("aaaaa", "bba"));
    	assertEquals(0, solution.strStr("aaaaa", ""));
    	assertEquals(0, solution.strStr("a", "a"));
    	assertEquals(1, solution.strStr("mississippi", "issi"));
    }
}
