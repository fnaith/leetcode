package leetcode.scope3.q23;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import leetcode.scope3.q23.Solution.ListNode;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {1, 1, 2, 3, 4, 4, 5, 6}, solution.listToArray(solution.mergeKLists(
    		new ListNode[] {solution.arrayToList(new int[] {1, 4, 5}),
    		solution.arrayToList(new int[] {1, 3, 4}), solution.arrayToList(new int[] {2, 6})})));
    	assertArrayEquals(new int[] {}, solution.listToArray(solution.mergeKLists(
        		new ListNode[] {})));
    }
}
