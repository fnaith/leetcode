package leetcode.scope3.q22;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals("[((())), (()()), (())(), ()(()), ()()()]", solution.generateParenthesis(3).toString());
    }
}
