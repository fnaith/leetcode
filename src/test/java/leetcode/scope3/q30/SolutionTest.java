package leetcode.scope3.q30;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals("[0, 9]", solution.findSubstring("barfoothefoobarman", new String[] {"foo","bar"}).toString());
    	assertEquals("[]", solution.findSubstring("wordgoodgoodgoodbestword", new String[] {"word","good","best","word"}).toString());;
    	assertEquals("[8]", solution.findSubstring("wordgoodgoodgoodbestword", new String[] {"word","good","best","good"}).toString());
    	assertEquals("[]", solution.findSubstring("", new String[] {}).toString());
    }
}
