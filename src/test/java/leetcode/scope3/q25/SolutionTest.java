package leetcode.scope3.q25;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {2, 1, 4, 3, 5}, solution.listToArray(solution.reverseKGroup(
    			solution.arrayToList(new int[]{1, 2, 3, 4, 5}), 2)));
    	assertArrayEquals(new int[] {3, 2, 1, 4, 5}, solution.listToArray(solution.reverseKGroup(
    			solution.arrayToList(new int[]{1, 2, 3, 4, 5}), 3)));
    	assertArrayEquals(new int[] {1}, solution.listToArray(solution.reverseKGroup(
    			solution.arrayToList(new int[]{1}), 2)));
    }
}
