package leetcode.scope3.q27;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(2, solution.removeElement(new int[]{3, 2, 2, 3}, 3));
    	assertEquals(5, solution.removeElement(new int[]{0, 1, 2, 2, 3, 0, 4, 2}, 2));
    }
}
