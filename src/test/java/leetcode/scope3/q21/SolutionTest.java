package leetcode.scope3.q21;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {1, 1, 2, 3, 4, 4}, solution.listToArray(solution.mergeTwoLists(
    		solution.arrayToList(new int[] {1, 2, 4}), solution.arrayToList(new int[] {1, 3, 4}))));
    }
}
