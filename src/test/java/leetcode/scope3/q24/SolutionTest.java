package leetcode.scope3.q24;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {2, 1, 4, 3}, solution.listToArray(solution.swapPairs(
    			solution.arrayToList(new int[]{1, 2, 3, 4}))));
    }
}
