package leetcode.scope2.q13;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals(3, solution.romanToInt("III"));
		assertEquals(4, solution.romanToInt("IV"));
		assertEquals(9, solution.romanToInt("IX"));
		assertEquals(58, solution.romanToInt("LVIII"));
		assertEquals(1994, solution.romanToInt("MCMXCIV"));
	}
}
