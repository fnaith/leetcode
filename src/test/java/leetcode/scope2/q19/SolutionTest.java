package leetcode.scope2.q19;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {1, 2, 3, 5}, solution.listToArray(solution.removeNthFromEnd(solution.arrayToList(new int[] {1, 2, 3, 4, 5}), 2)));
    	assertArrayEquals(new int[] {}, solution.listToArray(solution.removeNthFromEnd(solution.arrayToList(new int[] {1}), 1)));
    	assertArrayEquals(new int[] {2}, solution.listToArray(solution.removeNthFromEnd(solution.arrayToList(new int[] {1, 2}), 2)));
	}
}
