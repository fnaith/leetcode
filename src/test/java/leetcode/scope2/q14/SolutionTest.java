package leetcode.scope2.q14;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("fl", solution.longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
		assertEquals("", solution.longestCommonPrefix(new String[]{"dog", "racecar", "car"}));
	}
}
