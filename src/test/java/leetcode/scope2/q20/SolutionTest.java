package leetcode.scope2.q20;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(true, solution.isValid("()"));
    	assertEquals(true, solution.isValid("()[]{}"));
    	assertEquals(false, solution.isValid("(]"));
    	assertEquals(false, solution.isValid("([)]"));
    	assertEquals(true, solution.isValid("{[]}"));
	}
}
