package leetcode.scope2.q15;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("[[-1, -1, 2], [-1, 0, 1]]", solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4}).toString());
		assertEquals("[[0, 0, 0]]", solution.threeSum(new int[]{0, 0, 0, 0}).toString());
		assertEquals("[[-1, 0, 1]]", solution.threeSum(new int[]{1, -1, -1, 0}).toString());
	}
}
