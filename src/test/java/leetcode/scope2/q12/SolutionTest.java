package leetcode.scope2.q12;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("III", solution.intToRoman(3));
		assertEquals("IV", solution.intToRoman(4));
		assertEquals("IX", solution.intToRoman(9));
		assertEquals("LVIII", solution.intToRoman(58));
		assertEquals("MCMXCIV", solution.intToRoman(1994));
	}
}
