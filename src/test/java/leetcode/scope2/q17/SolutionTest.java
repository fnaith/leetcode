package leetcode.scope2.q17;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("[ad, ae, af, bd, be, bf, cd, ce, cf]", solution.letterCombinations("23").toString());
		assertEquals("[]", solution.letterCombinations("").toString());
	}
}
