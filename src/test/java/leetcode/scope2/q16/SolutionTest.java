package leetcode.scope2.q16;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals(2, solution.threeSumClosest(new int[]{-1, 2, 1, -4}, 1));
		assertEquals(3, solution.threeSumClosest(new int[]{0, 1, 2}, 3));
		assertEquals(3, solution.threeSumClosest(new int[]{0, 1, 2}, 0));
		assertEquals(-2, solution.threeSumClosest(new int[]{-3, -2, -5, 3, -4}, -1));
	}
}
