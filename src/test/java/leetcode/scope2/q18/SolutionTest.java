package leetcode.scope2.q18;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("[[-2, -1, 1, 2], [-2, 0, 0, 2], [-1, 0, 0, 1]]", solution.fourSum(new int[]{1, 0, -1, 0, -2, 2}, 0).toString());
		assertEquals("[[-3, -1, 0, 4]]", solution.fourSum(new int[]{-3, -1, 0, 2, 4, 5}, 0).toString());
		assertEquals("[[-4, 0, 1, 2], [-1, -1, 0, 1]]", solution.fourSum(new int[]{-1, 0, 1, 2, -1, -4}, -1).toString());
		assertEquals("[[-5, -4, -1, 1], [-5, -4, 0, 0], [-5, -2, -2, 0], [-4, -2, -2, -1]]",
			solution.fourSum(new int[]{-1, 0, -5, -2, -2, -4, 0, 1, -2}, -9).toString());
	}
}
