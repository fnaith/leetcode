package leetcode.scope4.q39;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals("[[3, 2, 2], [7]]", solution.combinationSum(new int[] {2, 3, 6, 7}, 7).toString());
    	assertEquals("[[2, 2, 2, 2], [3, 3, 2], [5, 3]]", solution.combinationSum(new int[] {2, 3, 5}, 8).toString());
    	assertEquals("[]", solution.combinationSum(new int[] {2}, 1).toString());
    	assertEquals("[[3, 2, 2], [5, 2]]", solution.combinationSum(new int[] {2, 3, 5}, 7).toString());
    }
}
