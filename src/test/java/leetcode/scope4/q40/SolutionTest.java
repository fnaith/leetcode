package leetcode.scope4.q40;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals("[[5, 2, 1], [6, 1, 1], [6, 2], [7, 1]]", solution.combinationSum2(new int[] {10, 1, 2, 7, 6, 1, 5}, 8).toString());
    	assertEquals("[[2, 2, 1], [5]]", solution.combinationSum2(new int[] {2, 5, 2, 1, 2}, 5).toString());
    }
}
