package leetcode.scope4.q32;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(2, solution.longestValidParentheses("(()"));
    	assertEquals(4, solution.longestValidParentheses(")()())"));
    }
}
