package leetcode.scope4.q31;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {1, 3, 2}, solution.testNextPermutation(new int[] {1, 2, 3}));
    	assertArrayEquals(new int[] {1, 2, 3}, solution.testNextPermutation(new int[] {3, 2, 1}));
    	assertArrayEquals(new int[] {1, 5, 1}, solution.testNextPermutation(new int[] {1, 1, 5}));
    	assertArrayEquals(new int[] {2, 1, 3}, solution.testNextPermutation(new int[] {1, 3, 2}));
    }
}
