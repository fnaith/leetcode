package leetcode.scope4.q33;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(4, solution.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
    	assertEquals(-1, solution.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3));
    	assertEquals(4, solution.search(new int[]{5, 1, 2, 3, 4}, 4));
    	assertEquals(2, solution.search(new int[]{5, 1, 3}, 3));
    }
}
