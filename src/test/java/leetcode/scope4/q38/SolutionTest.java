package leetcode.scope4.q38;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals("1", solution.countAndSay(1));
    	assertEquals("1211", solution.countAndSay(4));
    }
}
