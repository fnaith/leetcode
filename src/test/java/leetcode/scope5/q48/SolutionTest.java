package leetcode.scope5.q48;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {
    @Test
    public void test() {
        Solution solution = new Solution();

        int[][] ex1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        solution.rotate(ex1);
        assertEquals("[7, 4, 1][8, 5, 2][9, 6, 3]", fromMatrix(ex1));

        int[][] ex2 = {{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}};
        solution.rotate(ex2);
        assertEquals("[15, 13, 2, 5][14, 3, 4, 1][12, 6, 8, 9][16, 7, 10, 11]", fromMatrix(ex2));
    }

    private String fromMatrix(int[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int[] row : matrix){
            sb.append(Arrays.toString(row));
        }
        return sb.toString();
    }
}
