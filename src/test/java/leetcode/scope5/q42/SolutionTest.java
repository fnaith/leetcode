package leetcode.scope5.q42;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(6, solution.trap(new int[] {0,1,0,2,1,0,1,3,2,1,2,1}));
    }
}
