package leetcode.scope5.q41;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertEquals(3, solution.firstMissingPositive(new int[] {1, 2, 0}));
    	assertEquals(2, solution.firstMissingPositive(new int[] {3, 4, -1, 1}));
    	assertEquals(1, solution.firstMissingPositive(new int[] {7, 8, 9, 11, 12}));
    	assertEquals(1, solution.firstMissingPositive(new int[] {}));
    	assertEquals(2, solution.firstMissingPositive(new int[] {1}));
    	assertEquals(2, solution.firstMissingPositive(new int[] {1, 1}));
    }
}
