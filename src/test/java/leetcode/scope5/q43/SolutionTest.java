package leetcode.scope5.q43;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	//assertEquals("6", solution.multiply("2", "3"));
    	assertEquals("56088", solution.multiply("123", "456"));
    }
}
