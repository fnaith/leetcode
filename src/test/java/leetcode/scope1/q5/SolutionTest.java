package leetcode.scope1.q5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals("bab", solution.longestPalindrome("babad"));
		assertEquals("bb", solution.longestPalindrome("cbbd"));
	}
}
