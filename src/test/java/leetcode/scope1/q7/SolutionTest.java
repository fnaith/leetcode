package leetcode.scope1.q7;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();
		
		assertEquals(321, solution.reverse(123));
		assertEquals(-321, solution.reverse(-123));
		assertEquals(21, solution.reverse(120));
		assertEquals(0, solution.reverse(1534236469));
		assertEquals(0, solution.reverse(-2147483648));
	}
}
