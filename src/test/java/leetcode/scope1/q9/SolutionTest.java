package leetcode.scope1.q9;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();
		
		assertEquals(true, solution.isPalindrome(121));
		assertEquals(false, solution.isPalindrome(-121));
		assertEquals(false, solution.isPalindrome(10));
	}
}
