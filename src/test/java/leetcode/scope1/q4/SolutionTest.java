package leetcode.scope1.q4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();
		
		assertEquals(2, solution.findMedianSortedArrays(new int[] {1, 3}, new int[] {2}));
		assertEquals(2.5, solution.findMedianSortedArrays(new int[] {1, 2}, new int[] {3, 4}));
		assertEquals(1.5, solution.findMedianSortedArrays(new int[] {1, 2}, new int[] {1, 2}));
	}
}
