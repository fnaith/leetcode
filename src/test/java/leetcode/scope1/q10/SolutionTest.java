package leetcode.scope1.q10;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

		assertEquals(false, solution.isMatch("aa", "a"));
		assertEquals(true, solution.isMatch("aa", "a*"));
		assertEquals(true, solution.isMatch("ab", ".*"));
		assertEquals(true, solution.isMatch("aab", "c*a*b"));
		assertEquals(false, solution.isMatch("mississippi", "mis*is*p*."));
		assertEquals(false, solution.isMatch("ab", ".*c"));
		assertEquals(true, solution.isMatch("mississippi", "mis*is*ip*."));
		assertEquals(true, solution.isMatch("aaa", "a*a"));
		assertEquals(true, solution.isMatch("a", "ab*"));
		assertEquals(true, solution.isMatch("aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*a*a*b"));
	}
}
