package leetcode.scope1.q2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();

    	assertArrayEquals(new int[] {8, 0, 7}, solution.listToArray(solution.addTwoNumbers(
    			solution.arrayToList(new int[] {3, 4, 2}),
    			solution.arrayToList(new int[] {4, 6, 5}))));
    	assertArrayEquals(new int[] {1, 8}, solution.listToArray(solution.addTwoNumbers(
    			solution.arrayToList(new int[] {1, 8}),
    			solution.arrayToList(new int[] {0}))));
	}
}
