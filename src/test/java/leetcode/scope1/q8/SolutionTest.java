package leetcode.scope1.q8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test() {
		Solution solution = new Solution();
		
		assertEquals(42, solution.myAtoi("42"));
		assertEquals(-42, solution.myAtoi("   -42"));
		assertEquals(4193, solution.myAtoi("4193 with words"));
		assertEquals(0, solution.myAtoi("words and 987"));
		assertEquals(-2147483648, solution.myAtoi("-91283472332"));
		assertEquals(0, solution.myAtoi(""));
		assertEquals(0, solution.myAtoi("+"));
		assertEquals(3, solution.myAtoi("3.14159"));
		assertEquals(0, solution.myAtoi("+-2"));
	}
}
