package leetcode.week154;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	@Test
	void test_maxNumberOfBalloons() {
		Solution solution = new Solution();

    	assertEquals(2, solution.maxNumberOfBalloons("loonbalxballpoon"));
    	assertEquals(0, solution.maxNumberOfBalloons("leetcode"));
    }
	
	@Test
	void test_reverseParentheses() {
		Solution solution = new Solution();

    	assertEquals("dcba", solution.reverseParentheses("(abcd)"));
    	assertEquals("iloveu", solution.reverseParentheses("(u(love)i)"));
    	assertEquals("leetcode", solution.reverseParentheses("(ed(et(oc))el)"));
    	assertEquals("apmnolkjihgfedcbq", solution.reverseParentheses("a(bcdefghijkl(mno)p)q"));
    	assertEquals("tauswa", solution.reverseParentheses("ta()usw((((a))))"));
    }
	
	@Test
	void test_kConcatenationMaxSum() {
		Solution solution = new Solution();

    	assertEquals(9, solution.kConcatenationMaxSum(new int[]{1, 2}, 3));
    	assertEquals(2, solution.kConcatenationMaxSum(new int[]{1, -2, 1}, 5));
    	assertEquals(0, solution.kConcatenationMaxSum(new int[]{-1, -2}, 7));
    	assertEquals(5, solution.kConcatenationMaxSum(new int[]{-5, 4, -4, -3, 5, -3}, 3));
    	assertEquals(4, solution.kConcatenationMaxSum(new int[]{2, -5, 1, 0, -2, -2, 2}, 2));
    }
}
