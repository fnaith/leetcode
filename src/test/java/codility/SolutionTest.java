package codility;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {
    @Test
    public void test() {
        Solution solution = new Solution();

        assertEquals(37, solution.solution1(28));
        assertEquals(743, solution.solution1(734));
        assertEquals(2089, solution.solution1(1990));
        assertEquals(10000, solution.solution1(1000));

        assertEquals(4, solution.solution2(new int[]{5, 4, 0, 3, 1, 6, 2}));

        assertEquals(3, solution.solution3(new int[]{13,7,2,8,3}));
        assertEquals(1, solution.solution3(new int[]{1, 2, 4, 8}));
        assertEquals(2, solution.solution3(new int[]{16, 16}));
    }
}
